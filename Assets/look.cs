﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class look : MonoBehaviour {
    public float myCameraMoveSpeed ;
    // Use this for initialization
    void Start () {
        myCameraMoveSpeed = 5f;

    }
	
	// Update is called once per frame
	void Update () {
        Vector3 cc = Input.mousePosition;
        if (cc.x < 10)
        {
            Vector3 B = gameObject.transform.position;
            B.x -= Time.deltaTime * myCameraMoveSpeed;
            gameObject.transform.position = B;
        }
        if (cc.x > Screen.width - 10)
        {
            Vector3 B1 = gameObject.transform.position;
            B1.x += Time.deltaTime * myCameraMoveSpeed;
            gameObject.transform.position = B1;
        }

        if (cc.y < 10)
        {
            Vector3 B2 = gameObject.transform.position;
            B2.z -= Time.deltaTime * myCameraMoveSpeed;
            gameObject.transform.position = B2;
        }

        if (cc.y > Screen.width - 10)
        {
            Vector3 B3 = gameObject.transform.position;
            B3.z += Time.deltaTime * myCameraMoveSpeed;
            gameObject.transform.position = B3;
        }
    }
}
