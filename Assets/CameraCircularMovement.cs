﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCircularMovement : MonoBehaviour
{
    public GameObject player;
    Vector3 zero;


    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position -  Vector3.forward * 10, 0.5f );
    }
}
