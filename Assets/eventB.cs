﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;///加上這一行，才能使用UI相關元件

public class eventB : MonoBehaviour
{

    private int time = 0;//點擊按鍵次數

    public Text valuetext;//每次點擊時，要改變的Text(0、1、2、3、.........)

    public void btnclick()
    {
        valuetext.text = (++time).ToString();//當執行點擊動作，先+1再把點擊次數轉成字串給valuetext(顯示在UI的數字)
    }
}
