﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;//要加這行

public class test2 : MonoBehaviour
{

    public Button pressbtn;//按鈕
    private Text presstext;//按鈕上的文字

    private bool show = false;//是否顯示圖片

    public RawImage image1;//rawimage
    public Texture imagefor2;
    public Texture imagefor1;//要顯示的圖片

    void Start()//初始狀態
    {
        presstext = pressbtn.GetComponentsInChildren<Transform>()[1].GetComponent<Text>();//從按鈕裡取得文字
        presstext.text = "顯示圖片";//初始文字設定為"顯示圖片"
    }
    public void btnclick()
    {
        if (show)
        {
            image1.texture = imagefor2;
            presstext.text = "顯示圖片";
        }
        else
        {
            image1.texture = imagefor1;
            presstext.text = "關閉圖片";
        }
        show = !show;
    }
}

