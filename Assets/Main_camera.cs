﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
public class Main_camera : MonoBehaviour {
    public float mouse_X ;
    public float mouse_Y;
    public Flowchart talk;
    public float mouse_scroll ;
    public float myCameraMoveSpeed = 1000f ;
    public GameObject  player  ;
    public GameObject point; 
    // Use this for initialization
    void Start () {
        mouse_scroll = 0;

        myCameraMoveSpeed = 60f;

}

// Update is called once per frame


void Update () {
        if (talk.HasExecutingBlocks() == false )
        {
            Vector3 a = Input.mousePosition;
            mouse_scroll = Input.GetAxis("Mouse ScrollWheel");
            if (mouse_scroll != 0)
            {
                transform.Translate(new Vector3(0, 0, mouse_scroll * Time.deltaTime * 200f), Space.Self);
            }
            /*
                    if ( Input.GetMouseButton(1))
                    {
                        mouse_X = Input.GetAxis("Mouse X");
                        mouse_Y = Input.GetAxis("Mouse Y");
                        Camera.main.transform.RotateAround(point.transform.position, point.transform.up, mouse_X * 20f);
                        Camera.main.transform.RotateAround(point.transform.position, transform.right, -1 * mouse_Y * 20f);
                    }
            */
            if (a.x < 10)
            {
                Vector3 B = gameObject.transform.localPosition;
                B.x -= Time.deltaTime * myCameraMoveSpeed;
                gameObject.transform.localPosition = B;
            }
            if (a.x > Screen.width - 10)
            {
                Vector3 B1 = gameObject.transform.localPosition;
                B1.x += Time.deltaTime * myCameraMoveSpeed;
                gameObject.transform.localPosition = B1;
            }

            if (a.y < 10)
            {
                Vector3 B2 = gameObject.transform.localPosition;
                B2.z -= Time.deltaTime * myCameraMoveSpeed;
                gameObject.transform.localPosition = B2;
            }

            if (a.y > Screen.width - 10)
            {
                Vector3 B3 = gameObject.transform.localPosition;
                B3.z += Time.deltaTime * myCameraMoveSpeed;
                gameObject.transform.localPosition = B3;
            }

        }
    }
}
