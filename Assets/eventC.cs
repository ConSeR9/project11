﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class eventC : MonoBehaviour
{

    public Toggle showtoggle;
    public InputField inputtext;
    public Text showtext;

    private bool show = false; //布林值控制顯示/關閉

    // Use this for initialization
    void Start()
    {  //初始狀態設置關閉
        showtoggle.isOn = false;
        inputtext.gameObject.SetActive(false);
        showtext.gameObject.SetActive(false);
    }

    public void toggleclick() //toggle點擊的函式
    {
        if (show)
        {
            inputtext.gameObject.SetActive(true);
            showtext.gameObject.SetActive(true);
        }
        else
        {
            inputtext.gameObject.SetActive(false);
            showtext.gameObject.SetActive(false);
        }
        show = !show;
    }
    public void textchange()//隨輸入的文字改變
    {
        showtext.text = inputtext.text;
    }
}
