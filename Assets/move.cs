﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
public class move : MonoBehaviour {
    public Animator player;
    public bool running;
    public bool waiting;
    public Flowchart talk;
    public float player_speed;
    public Camera main_camear;
    public Ray ray;
    public GameObject point;
    // Use this for initialization
    void Start () {
        player = GetComponent<Animator>();
        running = false;
        waiting = true;
        player_speed = 3f;
	}

    // Update is called once per frame
    void Update()
    {
        if (talk.HasExecutingBlocks() == false)
        {

            if (Input.GetMouseButtonDown(0))
            {
                ray = main_camear.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                RaycastHit[] raycasthit = Physics.RaycastAll(ray, 50);
                for (int i = 0; i < raycasthit.Length; i++)
                {
                    if (raycasthit[i].collider.tag == "floor")
                    {
                        point.transform.position = raycasthit[i].point;
                        this.transform.LookAt(point.transform);
                        this.transform.eulerAngles = new Vector3(0, this.transform.eulerAngles.y, 0);

                        set_allstate_flase();
                        running = true;

                    }
                }
            }


            if (running == true)
            {
                if (Mathf.Abs((this.transform.position.x - point.transform.position.x)) < 0.1f && Mathf.Abs((this.transform.position.z - point.transform.position.z)) < 0.1f)
                {
                    running = false;
                    waiting = true;
                    player.Play("waiting");
                }
                else
                {
                    player.Play("running");
                    moving(player_speed);
                }
            }



            if (Input.GetKey("w"))
            {
                running = true;
                waiting = false;
                player.Play("running");
                transform.rotation = Quaternion.Euler(0, 0, 0);
                transform.Translate(new Vector3(0, 0, player_speed * 0.05f), Space.Self);
            }

            if (Input.GetKey("a"))
            {
                running = true;
                waiting = false;
                player.Play("running");
                transform.rotation = Quaternion.Euler(0, -90, 0);
                transform.Translate(new Vector3(0, 0, player_speed * 0.05f), Space.Self);
                //transform.rotation = Quaternion.Euler(0, -90, 0);
            }

            if (Input.GetKey("s"))
            {
                running = true;
                waiting = false;
                player.Play("running");
                transform.rotation = Quaternion.Euler(0, 180, 0);
                transform.Translate(new Vector3(0, 0, player_speed * 0.05f), Space.Self);
            }
            if (Input.GetKey("d"))
            {
                running = true;
                waiting = false;
                player.Play("running");
                transform.rotation = Quaternion.Euler(0, 90, 0);
                transform.Translate(new Vector3(0, 0, player_speed * 0.05f), Space.Self);

            }



            if (Input.GetKeyUp("w") || Input.GetKeyUp("a") || Input.GetKeyUp("s") || Input.GetKeyUp("d"))
            {

                waiting = true;
                running = false;
                player.Play("waiting");

            }

        }
     }

    void set_allstate_flase()
    {
        running = false;
        waiting = false;
    }
    void moving( float speed )
    {
        transform.Translate(new Vector3(0, 0, player_speed * Time.deltaTime), Space.Self);
    }
}
