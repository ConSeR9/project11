﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
public class Raycast : MonoBehaviour {
    public Camera main_camear;
    public Ray ray;
    public Flowchart talk;
    public GameObject point;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (talk.HasExecutingBlocks() == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ray = main_camear.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                RaycastHit[] raycasthit = Physics.RaycastAll(ray, 50);
                for (int i = 0; i < raycasthit.Length; i++)
                {
                    if (raycasthit[i].collider.tag == "floor")
                    {
                        point.transform.position = raycasthit[i].point;
                        this.transform.LookAt(point.transform);
                    }
                }
            }
        }
	}

}
